package cn.yang;

import org.junit.Test;

import javax.mail.*;
import javax.mail.internet.*;
import java.io.File;
import java.io.IOException;
import java.util.Properties;

/**
 * JavaMail第一例
 * Created by Yang on 2015/1/31
 */
public class Demo02 {
    @Test
    public void fun1() throws MessagingException {
        /**
         * 1.得到Session
         */
        Properties props = new Properties();
        props.setProperty("mail.host", "smtp.163.com");//设置邮件服务器地址
        props.setProperty("mail.smtp.auth", "true");//设置邮件服务器是否需要登录认证
        //创建认证器
        Authenticator auth = new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication("itcast_yang", "jazr932832");//指定用户名和密码

            }
        };

        Session session = Session.getInstance(props, auth);


        /**
         * 2.创建MimeMessage
         */
        MimeMessage msg = new MimeMessage(session);
        msg.setFrom(new InternetAddress("itcast_yang@163.com"));//设置发件人
        msg.addRecipients(Message.RecipientType.TO, "firm_yang@163.com");//设置收件人
        msg.addRecipients(Message.RecipientType.CC, "firm_yang@sohu.com");//设置收件人,类型为抄送
        msg.addRecipients(Message.RecipientType.BCC, "firm_yang@sina.com");//设置收件人,类型为暗送

        msg.setSubject("这是一封测试邮件");//设置邮件主题
        //指定内容,及内容的MIME类型
        msg.setContent("这是一封用来测试JavaMail的垃圾邮件", "text/html;charset=utf-8");

        /**
         * 3.发送
         */
        Transport.send(msg);
    }

    @Test
    public void fun2() throws MessagingException, IOException {
        /**
         * 1.得到Session
         */
        Properties props = new Properties();
        props.setProperty("mail.host", "smtp.163.com");//设置邮件服务器地址
        props.setProperty("mail.smtp.auth", "true");//设置邮件服务器是否需要登录认证
        //创建认证器
        Authenticator auth = new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication("itcast_yang", "jazr932832");//指定用户名和密码

            }
        };

        Session session = Session.getInstance(props, auth);


        /**
         * 2.创建MimeMessage
         */
        MimeMessage msg = new MimeMessage(session);
        msg.setFrom(new InternetAddress("itcast_yang@163.com"));//设置发件人
        msg.addRecipients(Message.RecipientType.TO, "firm_yang@163.com");//设置收件人
        msg.addRecipients(Message.RecipientType.CC, "firm_yang@sohu.com");//设置收件人,类型为抄送
        msg.addRecipients(Message.RecipientType.BCC, "firm_yang@sina.com");//设置收件人,类型为暗送

        msg.setSubject("又是一封测试邮件");//设置邮件主题

        //可以装载多个主体部件! 可以把它当成一个集合
        MimeMultipart partList = new MimeMultipart();
        msg.setContent(partList);//把邮件的内容设置为多部件的集合对象

        //创建一个部件,
        MimeBodyPart part1 = new MimeBodyPart();
        //给部件指定内容
        part1.setContent("又是一封垃圾邮件","text/html;charset=utf-8");
        //将部件添加到集合中
        partList.addBodyPart(part1);


        //又创建一个部件
        MimeBodyPart part2 = new MimeBodyPart();
        //为部件指定附件
        part2.attachFile(new File("f:/image/1.jpg"));
        part2.setFileName(MimeUtility.encodeText("大美女.jpg"));
        partList.addBodyPart(part2);

        /**
         * 3.发送
         */
        Transport.send(msg);
    }

}
